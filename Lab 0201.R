#Exercise 1
plot(iris$Petal.Length, iris$Petal.Width)
plot(subset(iris$Petal.Length, iris$Species == "setosa"), 
     subset(iris$Petal.Width, iris$Species == "setosa"), 
     xlab = "Petal Length", ylab = "Petal Width", main = "Setosa"
     )
iris$Speciespch [iris$Species == "setosa"] <- 1
iris$Speciespch [iris$Species == "virginica"] <- 2
iris$Speciespch [iris$Species == "versicolor"] <- 3
plot(iris$Petal.Length, iris$Petal.Width, pch = iris$Speciespch)
plot(iris$Petal.Length, iris$Petal.Width, col = iris$Speciespch + 5)
plot(iris$Petal.Length, iris$Petal.Width, 
     pch = iris$Sepal.Length, 
     col = iris$Speciespch + 5
     )
plot(iris$Petal.Length, iris$Petal.Width, pch = 16, 
     cex = iris$Sepal.Length - 4.5, 
     col = iris$Speciespch + 5
     )
scatter.smooth(iris$Petal.Length, iris$Petal.Width)
