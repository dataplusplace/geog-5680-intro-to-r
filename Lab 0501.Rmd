---
title: 'Lab 5.1: Spatial data in R'
author: "John Payne"
date: "5/10/2019"
output: html_document
---

## In this lab we'll look at some of the available mapping functionality

### Exercise 1

We'll start by loading our countries shapefile and plotting some of the fields to show comparisons between countries around the world

```{r}
library(sp)
library(maptools)
cntry <- readShapeSpatial("countries/countries.shp")
```

First, a look at median GDP, with a log transformation to smooth the distribution

```{r}
cntry$log.gdp <- log10(cntry$gdp_md_est)
spplot(cntry[,"log.gdp"])
```

Next, we'll look at population, again with a log transformation

```{r}
cntry$log.pop <- log10(cntry$pop_est)
spplot(cntry[,"log.pop"])
```

And lastly, we'll explore some of the color tools to show the ordinal income group data

```{r}
library(RColorBrewer)
plt <- brewer.pal(5, "YlOrRd")
spplot(cntry[,"income_grp"], col.regions = plt)
```


### Exercise 2

Let's look at the raster functionality

We'll load the NCAR NCEP monthly global temperature set and explore the data

```{r}
library(raster)
library(ncdf4)
library(maps)
r <- rotate(raster("air.mon.mean.nc", varname = "air"))
plot(r)
map(add = TRUE)
r
```

The file has multiple layers, so we'll pull them all and aggregate them at the monthly level

```{r}
r.stk = rotate(stack("air.mon.mean.nc", varname="air"))
r.stk
mth.count <- max(seq(1948:2015)) * 12 + 3
mth.names <- rep(c("Dec","Jan","Feb","Mar",
                   "Apr","May","Jun","Jul",
                   "Aug","Sep","Oct","Nov"
                   ),
                 length.out = mth.count
                 )
names(r.stk) <- paste("AirT",mth.names)
plot(r.stk, zlim=c(-30,30))
```

