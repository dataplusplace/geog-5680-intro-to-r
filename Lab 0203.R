#Exercise 1
squid <- read.csv("squid.csv")
hist(squid$GSI, 
     col = "grey", 
     xlab = "Squid GSI", 
     main = "Number of Squid by GSI",
)
mybreaks <- c(0:15)
hist(squid$GSI[squid$Sex == 2], 
     col = "pink", 
     xlab = "Squid GSI", 
     breaks = mybreaks, 
     xlim = range(squid$GSI), 
     ylim = range(0:650),
     main = "Number of Squid by Sex & GSI",
)
hist(squid$GSI[squid$Sex == 1], 
     col = "lightblue", 
     breaks = mybreaks, 
     xlim = range(squid$GSI), 
     add = TRUE,
)

#Exercise 2
boxplot(GSI ~ Sex, data = squid)
squid$GSI.log <- log(squid$GSI)
boxplot(GSI.log ~ Location, data = squid)

#Exercise 3
birdflu <- read.csv("BirdFlu.csv")
case.df <- data.frame(Year = c(2003:2008),
                      China = as.numeric(birdflu[4,c(2,4,6,8,10,12)]),
                      Egypt = as.numeric(birdflu[7,c(2,4,6,8,10,12)]),
                      Indonesia = as.numeric(birdflu[8,c(2,4,6,8,10,12)]),
                      Thailand = as.numeric(birdflu[13,c(2,4,6,8,10,12)]),
                      Vietnam = as.numeric(birdflu[15,c(2,4,6,8,10,12)])
                      )
plot(case.df$Year, case.df$China, 
     type = 'l', col = 1, lwd = 2,
     main = "Bird Flu Cases by Country",
     xlab = "Year", ylab = "Cases", ylim = c(0,65)
     )
lines(case.df$Year, case.df$Egypt, lwd = 2, col = 2)
lines(case.df$Year, case.df$Indonesia, lwd = 2, col = 3)
lines(case.df$Year, case.df$Thailand, lwd = 2, col = 4)
lines(case.df$Year, case.df$Vietnam, lwd = 2, col = 5)
legend("topright", legend = colnames(subset(case.df[,2:6])),
       col = c(1:5), lty = 1, lwd = 2, cex = 0.8
       )

#Exercise 4
msq.dens <- density(squid$GSI[squid$Sex == 1])
fsq.dens <- density(squid$GSI[squid$Sex == 2])
plot(msq.dens, xlim = c(-2,max(squid$GSI)), ylim = c(0,0.7), 
     main = "Squid GSI by Sex", xlab = "GSI", col = "blue", lwd = 2
     )
lines(fsq.dens, col = "maroon", lwd = 2)
