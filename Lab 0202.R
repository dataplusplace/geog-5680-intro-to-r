#Exercise 1a
giss <- read.csv("giss_temp.csv")

for (i in 1:12) {
  monthID = which(giss$Month == i)
  png(paste("giss_temp_",i,".png", sep = ''),
      width = 1024, height = 768
  )
  plot(giss$Year[monthID], giss$TempAnom[monthID],
       xlab = "Year", ylab = "Temperature Anomaly",
       main = paste(month.abb[i], "Temp Anomaly"),
       ylim = range(giss$TempAnom), pch = 16, col = 6
  )
  dev.off()
}
#Exercise 1b
for (i in 1:12) {
  monthID <- which(giss$Month == i)
  png(paste("giss_temp_line_",i,".png", sep = ''),
      width = 1024, height = 768
  )
  scatter.smooth(giss$Year[monthID], 
                 giss$TempAnom[monthID],
                 span = 1/10,
                 xlab = "Year", 
                 ylab = "Temperature Anomaly",
                 main = paste(month.abb[i], 
                              "Temp Anomaly"),
                 ylim = range(giss$TempAnom)
  )
  dev.off()
}
#Exercise 1c
Veg <- read.csv("Vegetation2.csv")
Veg$TtlPrec <- Veg$FallPrec + Veg$SprPrec + 
               Veg$SumPrec + Veg$WinPrec
Veg$YrF <- as.numeric(factor(Veg$Time))
for (i in 1:8) {
  yrID = which(Veg$YrF == i)
  png(paste("veg_precip_",i,".png", sep = ''),
      width = 1024, height = 768
  )
  plot(Veg$TtlPrec[yrID], Veg$R[yrID],
       xlab = "Annual Precipitation", 
       ylab = "Richness",
       main = paste(i, "Richness by Ann. Precip."),
       ylim = range(Veg$R), 
       xlim = range(Veg$TtlPrec),
       pch = 16, col = 6
  )
  dev.off()
}
