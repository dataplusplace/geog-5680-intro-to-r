#Exercise 1
Lengthct <- c(75, 85, 91.6, 95, NA, 105.5, 106)
Tb <- c(0, 0, 1, NA, 0, 0, 0)
mean(Lengthct, na.rm = TRUE)
#93.01667

#Exercise 2
Farm <- c("MO","MO","MO","MO","LN","SE","QM")
Month <- c(11,07,07,NA,09,09,11)
Boar <- cbind(Month, Lengthct, Tb)
Boar
dim(Boar)
Boar[1:nrow(Boar),]
Boar[,1:ncol(Boar)]

#Exercise 3
Year <- c(00,00,01,NA,03,03,02)
Sex <- c(1,2,2,2,1,2,2)
Lengthclass <- c(1,1,1,1,1,1,1)
Ecervi <- c(0,0,0,NA,0,0,0)
Boar.df <- data.frame(Frm = Farm, Mth = Month, 
                      Yr = Year, Sex = Sex, 
                      Len_cl = Lengthclass, 
                      Len_ct = Lengthct, 
                      Ecv = Ecervi, Tb = Tb)
Boar.df$Lsq <- sqrt(Boar.df$Len_ct)
Boar.list <- list(Frm = Farm, Mth = Month, 
                  Yr = Year, Sex = Sex, 
                  Len_cl = Lengthclass, 
                  Len_ct = Lengthct, 
                  Ecv = Ecervi, Tb = Tb)
Boar.list$Lsq <- sqrt(Boar.list$Len_ct)
summary(Boar.df)
summary(Boar.list)
head(Boar.df)
head(Boar.list)
#The list method keeps each element in a separate object and doesn't allow simple analysis

#Exercise 4
co2 <- read.csv("co2_mm_mlo.csv",na.strings = -99.99)
names(co2)
mean(co2$average, na.rm = TRUE)
#347.5297
