---
title: 'Lab 4.1: Advanced plotting with ggplot2'
author: "John Payne"
date: "5/9/2019"
output: html_document
---

## In this lab we'll examine the ggplot2 package

### Exercise 1

We'll start by plotting some visualizations of the global temperature data set

First, a look at the distribution of salinity in our sample

```{r}
temp <- read.csv("Temperature.csv")
library(ggplot2)
templot <- ggplot(temp, aes(x = Salinity))
templot + geom_histogram(bins = 50, na.rm = TRUE)
```

Next two time series split by year and by month

```{r}
templot + geom_histogram(color = 'blue', fill = 'white', bins = 50, na.rm = TRUE) + facet_wrap(~ Year)
templot + geom_histogram(color = 'blue', fill = 'white', bins = 50, na.rm = TRUE) + facet_wrap(~ Month)
```

Now, we'll split the data by station, generate boxplots for each and save the image down

```{r, fig.width = 12}
templot2 <- ggplot(temp, aes(x = Station, y = Temperature))
templot2 + geom_boxplot(na.rm = TRUE)
ggsave("temperature_by_station.png", templot2)
```

Lastly, sorting stations by median temperature

```{r, fig.width = 12}
templot2 + geom_boxplot(aes(x = reorder(Station, Temperature, median, na.rm = TRUE)), na.rm = TRUE)
```

### Exercise 2

Let's look at the scatter and line plot functionality in ggplot2

We'll start by creating a decimal date field so we can plot a time series

```{r}
temp$decdate <- temp$Year + temp$dDay3 / 365
```

Then look at a scatterplot of temperature and salinity with a time component

```{r}
splot <- ggplot(temp, aes(x = Temperature, y = Salinity, color = decdate))
splot + geom_point(na.rm = TRUE)
```

Next, salinity over time by station area

```{r}
sal.splot <- ggplot(temp, aes(x = decdate, y = Salinity, color = Temperature))
sal.splot + geom_point(na.rm = TRUE) + facet_wrap(~ Area)
```

Lastly, a line plot of salinity by station, saving the results to a pdf

```{r}
sal.lplot <- ggplot(temp, aes(x = decdate, y = Salinity, color = Temperature))
sal.lplot + geom_line(na.rm = TRUE) + facet_wrap(~ Station)
ggsave("Salinity over time by station.pdf", sal.lplot)
```
