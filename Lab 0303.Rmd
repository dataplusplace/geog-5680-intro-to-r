---
title: "Lab 3.3: Making Lab 2.1 Pretty"
author: "John Payne"
date: "May 8, 2019"
output: html_document
---
## For this lab we explore basic plotting functionality in R
We'll be using the Iris data set

### First a simple plot comparing petal length vs width
```{r}
plot(iris$Petal.Length, iris$Petal.Width)
```

### Next, the same plot for a subset of the data using the Setosa species
```{r}
plot(subset(iris$Petal.Length, iris$Species == "setosa"), 
     subset(iris$Petal.Width, iris$Species == "setosa"), 
     xlab = "Petal Length", ylab = "Petal Width", 
     main = "Setosa")
```

### Exploring some of the different symbols available

We'll generate a new column representing each species type to delineate the different symbols
```{r}
iris$Speciespch [iris$Species == "setosa"] <- 1
iris$Speciespch [iris$Species == "virginica"] <- 2
iris$Speciespch [iris$Species == "versicolor"] <- 3
```

And plot the petals again with the species shown using different symbols
```{r}
plot(iris$Petal.Length, iris$Petal.Width, 
     pch = iris$Speciespch)
```

### Displaying species using different colors
We'll repurpose the numeric column we generated since it can be adapted to generate semi-random non-primary colors
```{r}
plot(iris$Petal.Length, iris$Petal.Width, 
     col = iris$Speciespch + 5)
```     

### Displaying scaled symbols by sepal width and colored by species

```{r}
plot(iris$Petal.Length, iris$Petal.Width, pch = 16, 
     cex = iris$Sepal.Length - 4.5, 
     col = iris$Speciespch + 5)
```

### And lastly exploring the smoothing line function
```{r}

scatter.smooth(iris$Petal.Length, iris$Petal.Width)
```